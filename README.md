
This repository contains raw data files for the Lockhart and Stroke Error tests. The data includes information about students' performance in these tests, including pre-test, post-test, and follow-up scores for two groups: the experimental group and the control group.

## Lockhart Test Data

### File: Lockhart test.csv

#### Columns:
1. **Student's Number**: A unique identifier for each student participating in the test.
2. **Group Type**: Distinguishes between the two groups - experimental group and control group.
3. **Pre-test Score**: The player's best score for forehand drive strokes out of three attempts in the Pre-test (before training).
4. **Post-test Score**: The player's best score for forehand drive strokes out of three attempts in the Post-test (after twelve sessions of training).
5. **Follow-up Score**: The player's best score for forehand drive strokes out of three attempts in the Follow-up, conducted after 10 days without training.

## Stroke Error Test Data

### File: Stroke Error test.csv

#### Columns:
1. **Student's Number**: A unique identifier for each student participating in the test.
2. **Group Type**: Distinguishes between the two groups - experimental group and control group.
3. **Pre-test Errors**: The number of errors in the 30 forehand drives in the Pre-test (before training).
4. **Post-test Errors**: The number of errors in the 30 forehand drives in the Post-test (after twelve sessions of training).
5. **Follow-up Errors**: The player's best score for the number of errors in the 30 forehand drives in the Follow-up, conducted after 10 days without training.

## Data Usage

This data can be used for various analyses and research related to the Lockhart and Stroke Error tests. If you use this data, please ensure proper attribution and adhere to any licensing or usage restrictions.

## License

Please specify any applicable license for your data, such as a Creative Commons license or any other terms under which the data can be used.

## OSF Link

You can access the data and related resources on the Open Science Framework (OSF) platform via the following link: [OSF Link](https://osf.io/3mg7v/?view_only=495d3c82a8ea4587afedde197f56ddd6).

## Contributing

If you wish to contribute to this dataset or report issues, please follow the GitHub standard procedures for contributing to repositories.

## Acknowledgments

If there are any specific acknowledgments or references related to the data, please include them here.

For questions or clarifications, contact [forouzan.farzinnejad@hs-coburg.de].

Thank you for using this dataset!
